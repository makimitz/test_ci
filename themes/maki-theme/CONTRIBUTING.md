
# How to contribute

If you spot any bugs, please use [Issue Tracker](https://github.com/rhazdon/hugo-theme-maki-theme/issues) or if you want to add a new feature directly please create a new [Pull Request](https://github.com/rhazdon/hugo-theme-maki-theme/pulls).
