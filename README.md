# Maki blog

## TODO

* add something a lense or something for search engine
* menu fix for article as summary
* add a konami code for a pedobear
* make the blog responsive
* re enable GAnalytics
* test google analytics, check if it works properly

## Done

* add friend to the index footer
* download index picture locally (too long with a bad internet connection)
* change mouse hover in the menu : from underline to pretty ping transition
* reduce height of the menu
* add feature "last post" to the indext
* add search and tag feature
* css on result search page
* add a google analytics
* find something cool for the prompt -> cat index.html ?
* move article tag to top
* check why block code add big top padding -> code block class are called "highlight"
* check why block code can't be text selected -> same as above
* increase post width to 1100px instead of 800px
* center article image
* split existing big writeups into multiple writeups 
* add all my articles
* set white to background-color for img -> for transparency ones
* optimize search feature